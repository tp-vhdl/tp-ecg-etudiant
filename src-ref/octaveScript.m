% ECG telecharge de
%https://archive.physionet.org/cgi-bin/atm/ATM
%Echantillonne à 500Hz (F_Nyquist = 250Hz)
% Script OCTAVE (pas matlab...)

Fs = 500;     % Frequence d'echantillonnage
Fn = Fs/2;    % Frequence de Nyquist
figure(1)
T = csvread('./ADCSamplesOctave.csv');
subplot(2,3,1);plot(T(:,2));title('Raw ECG signal');xlabel('Samples (Fs=500Hz)');ylabel('Magnitude (output of an 11-bit ADC)');

% Pourc Octave (a supprimer sous Matlab)
pkg load signal;

%Pour les trois filtres suivants, on peut jouer sur les ordres
% donc le nombre de coefficients des filtres numeriques

%suppression de la baseline
fBaseLine=fir1(128, 5/Fn, 'high');
y_minus_BL=filter(fBaseLine,[1],T(:,2));
subplot(2,3,2);plot(y_minus_BL);title('Baseline wander reduced');xlabel('Samples (Fs=500Hz)');ylabel('Magnitude (digital signal)');
subplot(2,3,3);plot(y_minus_BL(1:1000));title('Baseline wander reduced -- zoomed');xlabel('Samples (Fs=500Hz)');ylabel('Magnitude (digital signal)');


%elimination du bruit à 50Hz par un coupe-bande tout basique
f50Hz=fir1(100, [45 55]/Fn, 'stop');
y_minus_50Hz_simple = filter(f50Hz,[1],y_minus_BL);
subplot(2,3,4);plot(y_minus_50Hz_simple(1:1000));title('FIR1 band-cut-- zoomed');xlabel('Samples (Fs=500Hz)');ylabel('Magnitude (digital signal)');

%elimination du bruit à 50Hz par un coupe-bande plus elabore
[b,a]=pei_tseng_notch ( 50 / Fn, 10/Fn );
y_minus_50Hz_pei_tseng = filter(b,a,y_minus_BL);
subplot(2,3,5);plot(y_minus_50Hz_pei_tseng(1:1000));title('Pei Tseng band-cut -- zoomed');xlabel('Samples (Fs=500Hz)');ylabel('Magnitude (digital signal)');

%lissage du bruit haute frequence par filtre de Parks-McClellan
Fpass  = 50;
Fstop = 60;
F     = [0 Fpass Fstop Fn]/(Fn);
A     = [1 1 0 0];
fLP = remez(10,F,A); % Voir pour Matlab: firpm
yLP = filter(fLP,[1],y_minus_50Hz_pei_tseng);

subplot(2,3,6);plot(yLP(1:1000));title('Low-pass filter to suppress high-freq noise -- zoomed');xlabel('Samples (Fs=500Hz)');ylabel('Magnitude (digital signal)');
figure(2)
subplot(2,1,1);plot(T(:,2));title('Raw ECG signal');xlabel('Samples (Fs=500Hz)');ylabel('Magnitude (digital signal)');
subplot(2,1,2);plot(yLP);title('After 3 filters');xlabel('Samples (Fs=500Hz)');ylabel('Magnitude (digital signal)');
print(2, "ECG_raw_3filters.pdf", "-dpdflatexstandalone");
figure(3)

%L'artillerie lourde: fonction intégrant la methode de Pan-Tompkin
%merci Sedghamiz. H !!!
pan_tompkin(T(:,2),500,1)





