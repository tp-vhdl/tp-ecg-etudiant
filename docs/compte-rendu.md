---
title: "Compte rendu Filtrage ECG"
author: John Doe
geometry: margin=1cm
output: pdf_document
mainfont: sans-serif
---

# Compte rendu du TP Carrefour

## Diagramme de la FSM

![Diagramme de la FSM](./img/FSM.png)

## Architecture de l'unité opérative

![Architecture de l'unité opérative](./img/OperativeUnit.png)

## Remarques

(Notez toute information qui vous semble pertinente)
