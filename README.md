# tp-ecg-etudiant

- Comment utiliser Git et Gitlab : [https://tp-vhdl.gitlab-pages.imt-atlantique.fr/gitlab/](https://tp-vhdl.gitlab-pages.imt-atlantique.fr/gitlab/)
- Énoncé du TP : [https://tp-vhdl.gitlab-pages.imt-atlantique.fr/ecg/](https://tp-vhdl.gitlab-pages.imt-atlantique.fr/ecg/)

## Questions

Vous devez compléter le diagramme de la FSM `docs/img/FSM.drawio` le schéma de l'architecture de l'unité opérative `docs/img/OperativeUnit.drawio` grâce à l'outil en ligne [https://app.diagrams.net/](Draw.io), en choisissant l'enregistrement sur périphérique. Il faut ensuite l'exporter au format PNG en écrasant le fichier PNG existant.

Notez dans le fichier `docs/compte-rendu.md` toute information que vous jugerez nécessaires.
